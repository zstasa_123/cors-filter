package com.thetransactioncompany.cors;

import static org.junit.Assert.*;

import org.junit.Test;


public class CORSRequestTypeTest
{
    @Test
    public void givenHostWithDefaultPortAndOriginWithNoPort_whenDeterminingRequestType_thenShouldReturnOther()
    {
        MockServletRequest request = new MockServletRequest();
        request.setHeader(HeaderName.HOST, "someserver.com:443");
        request.setHeader(HeaderName.ORIGIN, "https://someserver.com");
        request.setScheme("https");
        
        CORSRequestType corsRequestType = CORSRequestType.detect(request);
        
        assertEquals(corsRequestType, CORSRequestType.OTHER);
    }
    
    @Test
    public void givenHostWithNoPortAndOriginWithNoPort_whenDeterminingRequestType_thenShouldReturnOther()
    {
        MockServletRequest request = new MockServletRequest();
        request.setHeader(HeaderName.HOST, "someserver.com");
        request.setHeader(HeaderName.ORIGIN, "https://someserver.com");
        request.setScheme("https");
        
        CORSRequestType corsRequestType = CORSRequestType.detect(request);
        
        assertEquals(corsRequestType, CORSRequestType.OTHER);
    }
    
    @Test
    public void givenHostWithNoPortAndOriginWithDefaultPort_whenDeterminingRequestType_thenShouldReturnOther()
    {
        MockServletRequest request = new MockServletRequest();
        request.setHeader(HeaderName.HOST, "someserver.com");
        request.setHeader(HeaderName.ORIGIN, "https://someserver.com:443");
        request.setScheme("https");
        
        CORSRequestType corsRequestType = CORSRequestType.detect(request);
        
        assertEquals(corsRequestType, CORSRequestType.OTHER);
    }
    
    @Test
    public void givenHostWithNoPortAndOriginWithNonStandardPort_whenDeterminingRequestType_thenShouldReturnActual()
    {
        MockServletRequest request = new MockServletRequest();
        request.setHeader(HeaderName.HOST, "someserver.com");
        request.setHeader(HeaderName.ORIGIN, "https://someserver.com:123");
        request.setScheme("https");
        
        CORSRequestType corsRequestType = CORSRequestType.detect(request);
        
        assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
    
    @Test
    public void givenHttpsHostAndHttpOrigin_whenDeterminingRequestType_thenShouldReturnActual()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someserver.com");
    	request.setHeader(HeaderName.ORIGIN, "http://someserver.com:123");
    	request.setScheme("https");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
    
    @Test
    public void givenDifferentHostAndOrigin_whenDeterminingRequestType_thenShouldReturnActual()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someotherserver.com");
    	request.setHeader(HeaderName.ORIGIN, "https://someserver.com:123");
    	request.setScheme("https");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
    
    @Test
    public void givenNullHost_whenDeterminingRequestType_thenShouldReturnActual()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, null);
    	request.setHeader(HeaderName.ORIGIN, "https://someserver.com:123");
    	request.setScheme("https");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
    
    @Test
    public void givenNullOrigin_whenDeterminingRequestType_thenShouldReturnOther()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someserver.com");
    	request.setHeader(HeaderName.ORIGIN, null);
    	request.setScheme("https");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.OTHER);
    }
    
    @Test
    public void givenInvalidURLForOriginAndHost_whenDeterminingRequestType_thenShouldFallbackOnStringMatch()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someserver.com");
    	request.setHeader(HeaderName.ORIGIN, "notvalid://someserver.com");
    	request.setScheme("notvalid");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.OTHER);
    }
    
    @Test
    public void givenInvalidURLForOrigin_whenDeterminingRequestType_thenShouldFallbackOnStringMatch()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someserver.com");
    	request.setHeader(HeaderName.ORIGIN, "notvalid://someserver.com");
    	request.setScheme("https");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
    
    @Test
    public void givenPreflightRequest_whenDeterminingRequestType_thenShouldReturnPreflight()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someotherserver.com");
    	request.setHeader(HeaderName.ORIGIN, "https://someserver.com");
    	request.setHeader(HeaderName.ACCESS_CONTROL_REQUEST_METHOD, "something");
    	request.setScheme("https");
    	request.setMethod("OPTIONS");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.PREFLIGHT);
    }
    
    @Test
    public void givenRequestWithWrongMethodForPreflight_whenDeterminingRequestType_thenShouldReturnActual()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someotherserver.com");
    	request.setHeader(HeaderName.ORIGIN, "https://someserver.com");
    	request.setHeader(HeaderName.ACCESS_CONTROL_REQUEST_METHOD, "something");
    	request.setScheme("https");
    	request.setMethod("GET");
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
    
    @Test
    public void givenRequestWithNullMethodForPreflight_whenDeterminingRequestType_thenShouldReturnActual()
    {
    	MockServletRequest request = new MockServletRequest();
    	request.setHeader(HeaderName.HOST, "someotherserver.com");
    	request.setHeader(HeaderName.ORIGIN, "https://someserver.com");
    	request.setHeader(HeaderName.ACCESS_CONTROL_REQUEST_METHOD, "something");
    	request.setScheme("https");
    	request.setMethod(null);
    	
    	CORSRequestType corsRequestType = CORSRequestType.detect(request);
    	
    	assertEquals(corsRequestType, CORSRequestType.ACTUAL);
    }
}