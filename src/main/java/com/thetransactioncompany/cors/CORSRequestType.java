package com.thetransactioncompany.cors;


import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;


/**
 * Enumeration of the CORS request types.
 *
 * @author Vladimir Dzhuvinov
 * @author Brandon Murray
 */
public enum CORSRequestType {

	
	/**
	 * Simple / actual CORS request.
	 */
	ACTUAL,
	
	
	/**
	 * Preflight CORS request.
	 */
	PREFLIGHT,
	
	
	/**
	 * Other (non-CORS) request.
	 */
	OTHER;
	
	
	/**
	 * Detects the CORS type of the specified HTTP request.
	 *
	 * @param request The HTTP request to check. Must not be {@code null}.
	 *
	 * @return The CORS request type.
	 */
	public static CORSRequestType detect(final HttpServletRequest request) {

		if (request.getHeader(HeaderName.ORIGIN) == null) {

			// All CORS request have an Origin header
			return OTHER;
		}

		// Some browsers include the Origin header even when submitting
		// from the same domain. This is legal according to RFC 6454,
		// section-7.3
		if (request.getHeader(HeaderName.HOST) != null) {

			String originAsString = request.getHeader(HeaderName.ORIGIN);
			String hostAsString = request.getScheme() + "://" + request.getHeader(HeaderName.HOST);
			try {
				URL origin = new URL(originAsString);
				URL host = new URL(hostAsString);
				if (protocolMatches(origin, host) && hostMatches(origin, host) && portMatches(origin, host)) {
					return OTHER;
				}
			} catch (MalformedURLException e) {
				// Fallback on String handling
				if (hostAsString.equals(originAsString)) {
					return OTHER;
				}
			}
		}
		
		// We have a CORS request - determine type
		if (request.getHeader(HeaderName.ACCESS_CONTROL_REQUEST_METHOD) != null &&
		    request.getMethod()                                         != null &&
		    request.getMethod().equalsIgnoreCase("OPTIONS")                        ) {

			return PREFLIGHT;

		} else {
			
			return ACTUAL;
		}
	}

	
	private static boolean hostMatches(final URL origin, final URL host) {
		return origin.getHost().equals(host.getHost());
	}

	
	private static boolean protocolMatches(final URL origin, final URL host) {
		return origin.getProtocol().equals(host.getProtocol());
	}

	
	private static boolean portMatches(final URL origin, final URL host) {
		int originPort = origin.getPort() == -1 ? origin.getDefaultPort() : origin.getPort();
		int hostPort = host.getPort() == -1 ? host.getDefaultPort() : host.getPort();

		return originPort == hostPort;
	}
}
